package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"ys-keyvalue-store/handler"
	"ys-keyvalue-store/repository"
	"ys-keyvalue-store/service"
)

var (
	perm = 0666
)

func main() {
	fileName := "httpRequests.log"
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	logFile, err := os.OpenFile(fileName, os.O_WRONLY|os.O_CREATE|os.O_APPEND, os.FileMode(perm))
	if err != nil {
		panic(err)
	}

	log.SetOutput(logFile)
	defer logFile.Close()

	keyValueRepository := repository.NewKeyValueStoreRepository(map[string]string{})
	keyValueService := service.NewService(keyValueRepository)
	keyValueHandler := handler.NewHandler(keyValueService)
	keyValueService.LoadKeyValueStoreToMemory()
	go keyValueService.SaveKeyValuesToFile()

	mux := http.NewServeMux()
	mux.Handle("/api/key/", keyValueHandler)
	mux.Handle("/api/keyValues", keyValueHandler)

	err1 := http.ListenAndServe(fmt.Sprintf(":%s", port), mux)
	if err1 != nil {
		log.Fatalf("An error occurred when start the application") //nolint
	}
}
